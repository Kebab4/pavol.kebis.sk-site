I"�<p>Stále som perfekcionista a preto sa k niektorím veciam nedokopem, lebo sa mi zdá, že ak by som ich urobil teraz, urobil by som ich nedokonale.
A tak som sa rozhodol prelomiť tento ľad a napísať o úplnej zbytočnosti – komu, prečo a o čom píšem.</p>

<h1 id="komu-píšem">Komu píšem?</h1>
<p>V prvom rade <strong>sebe</strong>, lebo keby som chcel robiť osvetu, asi by som to zdieľal na sociálnych sieťach, dával si robiť gramatické korektúry, formálne citoval a kto vie aké zbytočnosti.
Niekto však môže namietať, prečo to potom dávam na internet, kde si to môže hocikto prečítať?
Ako môžem robiť niečo pre seba a zároveň to vystavovať do naha pred celým prepojeným technologickým svetom?
Odpovedí na túto námietku je niekoľko:</p>
<ol>
  <li>Snáď budú vďaka tomu moje <strong>texty kvalitnejšie</strong>. Keď už totiž niečo zverejním, tak to musí dávať zmysel – aby ma niekto úplne jednoducho neskritizoval. Keď nad niečím premýšľam sám v hlave, zvyknem robiť skratky alebo logické chyby, ktoré by som inak pri kontrolovaní odhalil.</li>
  <li>Keď raz napíšem niečo, čo bude <strong>hodné zdieľania</strong>, nemusím si kvôli tomu robiť vlastnú stránku a nemusím to robiť cez word-ovské dokumenty, facebook-ové statusy, či mailové vlákna.</li>
  <li>Možno to raz niekoho niekedy <strong>osloví a inšpiruje</strong>. Dúfam, že ak sa to niekedy stane, bude to len v tom najlepšom zmysle.</li>
</ol>

<p>Ale aby som bol úprimný, celkom by ma prekvapilo, ak by si tento konkrétny blog niekto do konca prečítal. Ak to urobíš obdivujem ťa. Ja by som to asi pri žiadnom inom takomto blogu nespravil.</p>

<h1 id="prečo-píšem">Prečo píšem?</h1>
<p>V hlave mám spomienku na to, ako som bol puberťák a neviem ako som to zo seba vylúdil, ale zrazu mi napadlo: <em>“Čo ak som jediný človek, ktorému chodia takéto veci po rozume a ktorý má premýšľanie? Čo ak všetci ostatní sú naprogramovaní a ja som jediná skutočne intelektuálna bytosť?”</em> Neviem, ako mi to vtedy napadlo, ale táto, skoro až descartovská úvaha, započala akési filozofické ja, ktoré vo mne počas rokov rástlo. Tým nechcem povedať, že si myslím, že som jediné intelektuálne bytie, ale že som začal viac uvažovať o princípoch a podstatných otázkach – o filozofii.</p>

<p>To sa najviac rozvinulo zrejme až počas dvoch rokov Kolégiu, kde som mal toľko otázok a úvah, že som si ich musel písať na papieriky – ale aj tak som na veľa z toho zabudol, alebo to postrácal. Každopádne som už mal viacero zaujímavých úvah o ktorých som si povedal: <em>“Toto by stálo dať niekam na papier.”</em> Ale nikdy som to neurobil. Som celkom presvedčený, že nie som jediný, kto má takéto úvahy a podľa mňa väčšina ľudí má aj akési “filozofické ja”, ktoré tam niekde dole drieme. Zo svojej skúsenosti mám však pocit, že väčšina našich úvah sú malichernosti, ktoré ak zabudneme, vôbec toho nebude škoda. Zároveň však budem omnoho radšej žiť vo svete, kde je extrémne veľa takýchto blogov, ktorých hodnota môže byť malá, ale ľudia sa učia premýšľať, ako keby takéto blogy vôbec neboli a vlastné uvažovanie by ostalo tak akurát pri troch vetách na pive, ktoré boli aj tak prehlušané jasajúcimi fanúšikmi Liveropoolu od vedľajšieho stola.</p>

<h1 id="námietka">Námietka</h1>

<p>Nie je to len ďalší krok k uzatváraniu spoločnosti plnej mudrlantov, ktorí sa s nikým nekonfrontujú a ktorí si myslia, že im nikto nerozumie? Áno, je to nebezpečie a ešte neviem ako sa s ním vysporiadam. Možno sa pridám do pisateľského krúžku, možno si nájdem parťákov s ktorým si raz za čas dám nejakú diskusiu. Uvidíme. Potrebujem sa však v tom písaní aj trošku natrénovať.</p>

<h1 id="čo-píšem">Čo píšem?</h1>
<p>To, čo ma baví a nad čím premýšľam. A to je:</p>
<ol>
  <li><strong>Filozofia a teológia</strong>. Ak sa niekto pýta, prečo som tu zahrnul teológiu, dôvod je jednoduchý: uvažovanie a písanie o teológií vyzerá skoro úplne rovnako ako uvažovanie a písanie o filozofii. Zároveň si myslím (ale to už je silné tvrdenie), že filozofia (najmä metafyzika) v konečnom dôsledku vedie k teológii. Teda pokiaľ má človek iba <em>filozofiu svojho života</em>, ešte nezavŕšil svoju cestu k <em>teológii svojho života</em>.</li>
  <li><strong>Lingvistické okienka</strong>. Niekoho to možno prekvapí, ale prienik informatiky a filozofie je jazyk. V informatike dokonca existuje také, že “formálne jazyky”. Nádherná vec! Každopádne, tieto okienka sú jednoducho jednohubky na príjemné nedeľné popoludnie, či na príliš strnulú diskusiu pri obede.</li>
  <li><strong>Vedy</strong>. Technické veci tu písať nebudem, snáď iba nejaké populárnejšie. Myslím si, že existujú ďaleko lepší popularizátori vedy, ktorí píšu úžasné články, videá a kto vie čo iné. No zároveň si myslím, že veda je stále málo porozumená a trošku viac popularizácie a poukázania na jej krásne zákutia nikdy nezaškodí.</li>
  <li><strong>Osobné</strong>. Toto sú vlastne high-level úvahy. A výrazom “high-level” nemyslím náročné, ale “nie fundamentálne”. Teda týkajúce sa bežného života, zážitkov, vzťahov, sebapoznania, atď.</li>
</ol>

<h1 id="upozornenia">Upozornenia</h1>
<ul>
  <li>Nie je tu možnosť komentovať. Možno raz pribudne, ale potrebujem si to ešte premyslieť, lebo diskusie na internete sú niekedy riadny odpad.</li>
  <li>Občas budem texty editovať aj po zverejnení (napríklad tento odsek tu v prvej verzii nebol).</li>
</ul>

<p><br />
Tak, ľady sú prelomené a môžeme ísť na to.</p>
:ET